﻿using System.Collections;
using UnityEngine;

public class EnvironmentController : EntityController {

    public override void Awake() {
        base.Awake();
        MouseController.environment.Add(this);
        var models = gameObject.transform.FindChild("Models");
        var iSelectedChild = Random.Range(0, models.childCount);
        for(int i = 0; i < models.childCount; i++) {
            models.GetChild(i).gameObject.SetActive(i == iSelectedChild);
        }
    }

    public void OnDisable() {
        MouseController.environment.Remove(this);
    }

    public void OnTriggerStay(Collider other) {
        var person = other.GetComponent<PersonController>();
        if(person != null) {
            person.OnTriggerStay(gameObject.GetComponent<Collider>());
        }
    }
}