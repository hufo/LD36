﻿using System;
using System.Collections;
using UnityEngine;

public class SoundEffectManager : MonoBehaviour {

    #region Wind
    public AudioClip soundEffectWind;
    public float soundEffectWindMaxVolume;
    #endregion Wind

    AudioSource audioSource;
    Coroutine soundRoutine;

    // Use this for initialization
    void Awake() {
        audioSource = GetComponent<AudioSource>();
        TimeManager.onDayTimeChanged += TimeManager_onDayTimeChanged;
    }

    private IEnumerator PlayWindSound(Season season) {
        while(true) {
            audioSource.clip = soundEffectWind;
            float targetPitch;
            switch(season) {
            case Season.Spring:
                targetPitch = .7f;
                break;
            case Season.Summer:
                targetPitch = .5f;
                break;
            case Season.Autumn:
                targetPitch = 1.2f;
                break;
            default:
            case Season.Winter:
                targetPitch = 1.5f;
                break;
            }
            audioSource.pitch = UnityEngine.Random.Range(targetPitch * .8f, targetPitch * 1.2f);
            audioSource.volume = UnityEngine.Random.Range(soundEffectWindMaxVolume / 4, soundEffectWindMaxVolume);
            audioSource.Play();
            yield return new WaitForSeconds(UnityEngine.Random.Range(4, 10));
        }
    }

    void TimeManager_onDayTimeChanged(TimeOfDay _dayTime, Season _season) {
        if(_dayTime == TimeOfDay.Morning) {
            soundRoutine = StartCoroutine(PlayWindSound(_season));
        } else {
            if(soundRoutine != null) {
                StopCoroutine(soundRoutine);
                soundRoutine = null;
            }
        }
    }

    // Update is called once per frame
    void Update() {
    }
}