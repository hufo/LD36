﻿using UnityEngine;

public class WoodFuel : MonoBehaviour
{
    public Fuel myFuel;

    private void Awake()
    {
        if (myFuel != null)
            myFuel = Instantiate<Fuel>(myFuel);
        else
            myFuel = ScriptableObject.CreateInstance<Fuel>();
    }

    private void Update()
    {
        myFuel.ApplyEnvironmentFactor();
    }

    public void ThrowOnFire(CampfireFueled _fire)
    {
        _fire.AddFuel(myFuel);
        Destroy(gameObject);
    }
}