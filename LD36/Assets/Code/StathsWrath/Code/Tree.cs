﻿using System.Collections;
using UnityEngine;

public class Tree : MonoBehaviour {
    public int amountOfWood = 3;
    public int chopsToFall = 3;
    public float rangeOfDrop = 2;
    public GameObject woodPrefab;

    public void Chop() {
        chopsToFall--;

        if(chopsToFall <= 0) {
            SpawnWoodAndDestroy();
        }
    }

    private void SpawnWoodAndDestroy() {
        float x, z;
        for(int i = 0; i < amountOfWood; i++) {
            x = Random.Range(-rangeOfDrop, rangeOfDrop);
            z = Random.Range(-rangeOfDrop, rangeOfDrop);

            var wood = Instantiate<GameObject>(woodPrefab);
            wood.transform.position = transform.position + new Vector3(x, 0, z);
        }
        GameObject.Destroy(gameObject);
    }
}