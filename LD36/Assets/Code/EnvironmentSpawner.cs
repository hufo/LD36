﻿using System.Collections;
using UnityEngine;

public class EnvironmentSpawner : MonoBehaviour {
    public GameObject tree;

    // Use this for initialization
    void Start() {
        for(int i = 0; i < 5; i++) {
            var x = Random.Range(-25, 25);
            var z = Random.Range(-25, 25);
            var newTree = (GameObject)Instantiate(tree, new Vector3(x, 0, z), Quaternion.identity);
            newTree.SetActive(true);
            newTree.transform.SetParent(transform);
        }
    }

    // Update is called once per frame
    void Update() {
    }
}