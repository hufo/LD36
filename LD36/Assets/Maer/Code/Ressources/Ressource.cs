﻿using System.Collections.Generic;
using UnityEngine;

public class Ressource : MonoBehaviour {
    public int curHitpoints = 10;
    public BasicWorker curWorker;
    public GameObject itemDrop;
    public int maxHitpoints = 10;
    private bool addedToList = false;
    GameObject highlight;

    public void harvest(int _strengh) {
        curHitpoints -= _strengh;

        if(curHitpoints <= 0)
            DestroyAndDrop();
    }

    public void OnMouseDown() {
        if(!addedToList) {
            highlight.SetActive(true);
            RessourceManager.me.AddRessource(this);
            addedToList = true;
        }
    }

    void Awake() {
        curHitpoints = maxHitpoints;
        highlight = transform.FindChild("Highlight").gameObject;
    }

    private void DestroyAndDrop() {
        curWorker.RessourceHarvestet();
        Instantiate(itemDrop, this.transform.position, Quaternion.identity);
        RessourceManager.me.RemoveRessource(this);
        Destroy(gameObject);
    }
}