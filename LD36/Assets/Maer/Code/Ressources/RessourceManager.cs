﻿using UnityEngine;
using System.Collections.Generic;

public class RessourceManager : MonoBehaviour {

    public List<Ressource> ressourceList = new List<Ressource>();

    public static RessourceManager me;

    void Awake()
    {
        me = this;
    }

    public void AddRessource(Ressource _ress)
    {
        ressourceList.Add(_ress);
    }

    public void RemoveRessource(Ressource _ress)
    {
        ressourceList.Remove(_ress);
    }

    public Ressource GetRessource()
    {
        foreach (Ressource ress in ressourceList)
        {
            if (ress.curWorker == null)
                return ress;
        }

        return null;
    }
}
